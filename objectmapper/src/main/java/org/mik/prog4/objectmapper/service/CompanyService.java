package org.mik.prog4.objectmapper.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.prog4.domain.Company;

public class CompanyService extends AbstractService<Company> {
    private static final Logger LOG= LogManager.getLogger();
      private final static Boolean DEBUG_TEMPORARY = true;

    static private CompanyService  instance;

    public static synchronized CompanyService getInstance() {
        if (instance==null)
            instance=new CompanyService();
        return instance;
    }

    private CompanyService() {

    }

    @Override
    void pay(Company client) {
        LOG.info(String.format("Enter CompanyService.pay, client:%s", client));
    }

    @Override
    void receiveService(Company client) {
        LOG.info(String.format("Enter CompanyService.receiveService, client:%s", client));
    }
}
