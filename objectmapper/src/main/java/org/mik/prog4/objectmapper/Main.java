package org.mik.prog4.objectmapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final Logger LOG= LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

   public static void main(String[] args) {
        try {
            Control c=new Control();
            c.jsonExport();
            c.xmlExport();
        }
        catch (Exception e) {
            LOG.error(e);
        }
    }
}
