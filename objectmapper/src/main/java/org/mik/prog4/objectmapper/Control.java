package org.mik.prog4.objectmapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.prog4.domain.*;

import org.mik.prog4.objectmapper.service.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Control {

    private static final Logger LOG = LogManager.getLogger();

    private PersonService personService=PersonService.getInstance();
    private CompanyService companyService=CompanyService.getInstance();
    private ObjectMapper objectMapper;
    private XmlMapper xmlMapper;

    private List<Client> dummies;

    public Control() {
        this.objectMapper= JsonMapper.builder()
                .addModules(new JavaTimeModule())
                .build();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
        objectMapper.setDateFormat(format);
        this.xmlMapper=XmlMapper.builder()
                .addModule(new JavaTimeModule())
                .build();
        xmlMapper.setDateFormat(format);
        dummies=createDummyList();
    }

    public String jsonExport() throws JsonProcessingException {
        String json=objectMapper.writeValueAsString(dummies);
        LOG.info(String.format("Json is %s", json));
        return json;
    }

    public String xmlExport() throws JsonProcessingException {
        String xml = xmlMapper.writeValueAsString(dummies);
        LOG.info(String.format("XM is %s", xml));
        return xml;
    }

    private List<Client> createDummyList() {
        List<Client> result=new ArrayList<>();
        Country hu=Country.builder()
                .name("Hungary")
                .sign("HU")
                .build();
        Country bet=Country.builder()
                .name("Betelgeuse")
                .sign("BET")
                .build();
        Country usa=Country.builder()
                .name("USA")
                .sign("US")
                .build();
        result.add(Person.builder()
                        .name("Zaphod Beeblebrox")
                        .country(bet)
                        .address("Betelgeuse main street 42")
                        .personalId("ZB12345")
                .build());
        result.add(Person.builder()
                .name("Tricia McMillan")
                .country(hu)
                .address("Hun main street 42")
                .personalId("TM12345")
                .build());
        result.add(Company.builder()
                .name("Google")
                .country(usa)
                .address("Frisco 42")
                .taxId("G12345")
                .build());
        return result;
    }

}
