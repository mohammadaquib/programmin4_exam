package org.mik.prog4.objectmapper.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.prog4.domain.Person;

public class PersonService extends AbstractService<Person> {
    private static final Logger LOG= LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    static private PersonService instance;

    public static synchronized PersonService getInstance() {
        if (instance==null)
            instance=new PersonService();
        return instance;
    }

    private PersonService() {}

    @Override
    void pay(Person client) {
        LOG.info(String.format("Enter personService.pay, client:%s", client));
    }

    @Override
    void receiveService(Person client) {
        LOG.info(String.format("Enter personService.receiveService, client:%s", client));
    }
}
