package org.mik.prog4.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Country.TBL_NAME)
@SequenceGenerator(name = "generator", sequenceName = "country_seq", allocationSize = 1)
public class Country extends AbstractEntity<Long>{

    public static final String TBL_NAME="country";
    public static final String FLD_NAME="name";
    public static final String FLD_SIGN="sign";

    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = FLD_NAME, nullable = false)
    private String name;

    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = FLD_SIGN, nullable = false)
    private String sign;

}
