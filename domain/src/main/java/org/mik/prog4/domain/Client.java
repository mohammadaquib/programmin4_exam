package org.mik.prog4.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Client.TBL_NAME)
@SequenceGenerator(name = "generator", sequenceName = "client_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Client extends AbstractEntity<Long> {

    public static final String TBL_NAME="client";
    public static final String FLD_NAME="name";
    public static final String FLD_ADDRESS="address";
    public static final String FLD_COUNTRY = "country";

    @NotNull(message = "Name cannot be empty")
    @Size(min = 2, max = 50, message = "name length must be between 2 and 50")
    @Column(name = FLD_NAME, nullable = false)
    private String name;

    @NotNull(message = "Address cannot be empty")
    @Size(min = 2, max = 50, message = "Address length must be between 2 and 50")
    @Column(name = FLD_ADDRESS, nullable = false)
    private String address;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @Column(name = FLD_COUNTRY, nullable = false)
    private Country country;

}
