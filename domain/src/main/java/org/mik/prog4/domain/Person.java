package org.mik.prog4.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Person.TBL_NAME)
@SequenceGenerator(name = "generator", sequenceName = "person_seq", allocationSize = 1)
public class Person extends  Client {

    public static final String TBL_NAME="person";
    public static final String FLD_PERSONAL_ID="personal_id";

    @NotNull
    @Size(min = 11, max = 11)
    @Column(name = FLD_PERSONAL_ID, nullable = false, unique = true)
    private String personalId;

}
