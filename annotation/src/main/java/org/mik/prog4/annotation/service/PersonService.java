package org.mik.prog4.annotation.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.prog4.annotation.domain.Person;

public class PersonService implements Service<Person> {

    private static final Logger LOG= LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private static PersonService instance;

    public static synchronized PersonService getInstance() {
        if (instance==null)
            instance=new PersonService();
        return instance;
    }

    private PersonService() {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter PersonService.PersonService");
    }

    @Override
    public void pay(Person client) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter PersonService.pay, client:"+client);
        System.out.println("Enter PersonService.pay, client:"+ client);
    }

    @Override
    public void receiveService(Person client) {
    if (DEBUG_TEMPORARY)
       LOG.debug("Enter PersonService.receiveService, client: "+client);
    System.out.println("Enter PersonService.receiveService, client: "+client);
    }


}
