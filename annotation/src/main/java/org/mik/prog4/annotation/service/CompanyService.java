package org.mik.prog4.annotation.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.prog4.annotation.domain.Company;


public class CompanyService implements Service<Company> {

    private static final Logger LOG=LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private static CompanyService instance;

    public static synchronized CompanyService getInstance() {
        if (instance == null)
            instance = new CompanyService();
        return instance;
    }

    private CompanyService() {
    }


    @Override
    public void pay(Company client) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter CompanyService.pay client:"+client);
        System.out.println("Enter CompanyService.pay client:"+client);
    }

    @Override
    public void receiveService(Company client) {
    if (DEBUG_TEMPORARY)
       LOG.debug("Enter CompanyService.receiveService client:"+client);
    System.out.println("Enter CompanyService.receiveService client:"+client);
    }
}
