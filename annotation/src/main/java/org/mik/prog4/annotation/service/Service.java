package org.mik.prog4.annotation.service;

import org.mik.prog4.annotation.domain.Client;

public interface Service<T extends Client> {

        void pay(T  client);

        void receiveService(T client);

}
