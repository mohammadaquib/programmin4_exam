package org.mik.prog4.annotation.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.mik.prog4.annotation.export.json.*;
import org.mik.prog4.annotation.export.xml.*;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@XMLSerializable
@JSonSerializable
public class Person extends Client {

    @XMLElement
    @JSonElement
    private String personalId;

}
