package org.mik.prog4.annotation.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.mik.prog4.annotation.export.json.*;
import org.mik.prog4.annotation.export.xml.*;



@Data
@NoArgsConstructor
@SuperBuilder
@XMLSerializable
@JSonSerializable
public class Client {
        @XMLElement
        @JSonElement
        private String name;
        @XMLElement
        @JSonElement
        private String address;
        @XMLElement
        @JSonElement
        private Country country;
}
