package org.mik.prog4.annotation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import org.mik.prog4.annotation.export.xml.XMLGenerator;
import org.mik.prog4.annotation.domain.*;
import org.mik.prog4.annotation.service.*;
import org.mik.prog4.annotation.export.json.*;

import java.util.ArrayList;
import java.util.List;


public class Control {

    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = true;

    private PersonService personService;
    private CompanyService companyService;
    private XMLGenerator xmlGenerator;
    private JSonGenerator jSonGenerator;
    private List<Client> clients;

    public Control() {
        personService=PersonService.getInstance();
        companyService=CompanyService.getInstance();
        xmlGenerator=new XMLGenerator();
        jSonGenerator=new JSonGenerator();
        this.clients=createDummyList();
    }

    private void dewIt(Client c) {
        if (c instanceof Person) {
            personService.pay((Person) c);
            return;
        }
        if (c instanceof Company) {
            companyService.pay((Company) c);
            return;
        }
        throw  new RuntimeException("Unknown client");
    }

    public void start() {
        System.out.println("Payment");
        clients.forEach(this::dewIt);

        System.out.println("XML generation");
        clients.forEach(client->
            System.out.println(this.xmlGenerator.convert2XML(client)));

        System.out.println("JSON generation");

        System.out.println("{ \"persons\": [ ");

        clients.forEach(client-> {
                System.out.println(jSonGenerator.convert2JSon(client));
                if (clients.indexOf(client)<clients.size()-1)
                    System.out.println(',');
            }
        );
        System.out.println("]}");
    }


    private List<Client> createDummyList() {
        List<Client> dummyList = new ArrayList<>();
        Country hu=Country.builder()
                .name("Hungary")
                .sign("HU")
                .build();
        Country usa=Country.builder()
                .name("United State of America")
                .sign("US")
                .build();
        dummyList.add(Person.builder()
                            .name("Zaphod Beeblebrox")
                            .address("Betelgeuse")
                            .country(hu)
                            .personalId("42234560TA")
                .build());
        dummyList.add(Person.builder()
                .name("Linus Torvalds")
                .address("Usa")
                .country(usa)
                .personalId("42234560TL")
                .build());
        dummyList.add(Person.builder()
                .name("Tricia McMillan")
                .address("London")
                .country(hu)
                .personalId("42234560TM")
                .build());
        dummyList.add(Person.builder()
                .name("Ford Prefect")
                .address("Betelgeuse")
                .country(hu)
                .personalId("42234560FP")
                .build());
        dummyList.add(Company.builder()
                .name("Google")
                .address("Frisco")
                .country(usa)
                .taxId("42234560GGG")
                .build());
        dummyList.add(Company.builder()
                .name("Microsoft")
                .address("Redmond")
                .country(usa)
                .taxId("42234560M$")
                .build());
        dummyList.add(Company.builder()
                .name("Oracle")
                .address("Washington")
                .country(usa)
                .taxId("42234560OR")
                .build());
        return dummyList;
    }
}
