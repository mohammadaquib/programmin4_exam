package org.mik.prog4.database.repository;

import org.hibernate.query.Query;
import org.mik.prog4.domain.Person;

import java.util.Queue;

public class PersonRepository extends AbstractRepository<Long, Person>{
    private static PersonRepository instance;
    public static synchronized PersonRepository getInstance(){
        if(instance==null)
            instance=new PersonRepository();
        return instance;
    }
    private PersonRepository(){

    }
    @Override
    protected Class<Person> getClazz() {
        return Person.class;
    }
    public Person getByPersonalId(String id) throws Exception{
        if(id==null || id.isEmpty())
            return null;
        return doIt(((session, tr) -> {
            Query<Person> query= (Query<Person>) createCriteria(session,Person.class,(cb, r, cq)->{
               cq.select(r).where(cb.equal(r.get(Person.FLD_PERSONAL_ID),Long.getLong(id)));
            });
        return query.getSingleResult();
        }));
    }
}
