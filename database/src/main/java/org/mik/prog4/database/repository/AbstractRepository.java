package org.mik.prog4.database.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.mik.prog4.database.hibernate.HibernateUtil;
import org.mik.prog4.domain.AbstractEntity;
import org.mik.prog4.domain.Client;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public abstract class AbstractRepository<ID extends Serializable,E extends AbstractEntity<ID>> {
    interface CriteriaSettings<R,T>{
        void Create(CriteriaBuilder cb, Root<T> root, CriteriaQuery<R> query);
    }
    interface SqlAction<R> {
        R process(Session session, Transaction tr) throws Exception;
    }
    protected abstract Class<E> getClazz();
    protected <R> R doIt(SqlAction<R> action)throws Exception{
        try(Session session= HibernateUtil.getSessionFactory().getCurrentSession()){
            Transaction tr=session.beginTransaction();
            try{
                return action.process(session,tr);
            }catch (Exception e){
                tr.rollback();
                throw  e;
            }finally {
                if(tr.getStatus().equals(TransactionStatus.ACTIVE))
                    tr.commit();
            }
        }
    }
    public void save(E entity)throws Exception{
        if(entity==null)
            return ;
        doIt(((session, tr) -> {
            session.save(entity);
            return entity;
        }));
    }
    public E getById(ID id) throws Exception{
        if(id==null)
            return null;
        return doIt(((session, tr) -> session.find(getClazz(),id)));
    }
    public List<E> getByName(String name)throws Exception{
        if(name==null || name.isEmpty())
            return Collections.emptyList();
        return doIt(((session,tr)->{
            Query<E> query=createCriteria(session,getClazz(),(cb, r, cq)->{
               cq.select(r).where(cb.equal(r.get(Client.FLD_NAME),name));
            });
            return query.getResultList();
        }));
    }
    public List<E>getAll() throws Exception{
        return doIt(((session,tr)->{
            org.hibernate.Query<E> query=createCriteria(session,getClazz(),(cb,r,cq)->{
                cq.select(r);
            });
            return query.getResultList();
        }));
    }


    protected <R>org.hibernate.Query<R> createCriteria (Session session,Class<R> resultClass,CriteriaSettings<R,E> criteriaSettings){
        CriteriaBuilder cb=session.getCriteriaBuilder();
        CriteriaQuery<R> cq=cb.createQuery(resultClass);
        Root<E> root=cq.from(getClazz());
        criteriaSettings.Create(cb,root,cq);
        return Session.createQuery(cq);
    }
}
